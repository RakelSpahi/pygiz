
class Rover:

    def __init__(self):
        self.pos = [0, 0]
        self.facing = 'n'

    def move_forward(self):
        if self.facing == 'n':
            self.pos[1] += 1

        elif self.facing == 'e':
            self.pos[0] +=1

        elif self.facing == 's':
            self.pos[1] -=1

        else:
            self.pos[0] -= 1

    def rotate_right(self):
        if self.facing == 'n':
            self.facing = 'e'

        elif self.facing == 'e':
            self.facing = 's'

        elif self.facing == 's':
            self.facing = 'w'

        else:
            self.facing = 'n'

    def rotate_left(self):
        if self.facing == 'n':
            self.facing = 'w'

        elif self.facing == 'w':
            self.facing = 's'

        elif self.facing == 's':
            self.facing = 'e'

        else:
            self.facing = 'n'

    def move_backward(self):
        if self.facing == 'n':
            self.pos[1] -= 1

        elif self.facing == 'e':
            self.pos[0] -=1

        elif self.facing == 's':
            self.pos[1] +=1

        else:
            self.pos[0] += 1

    def commands(self, cms):
        #  ['mf', 'mf', 'rr', 'mf', 'rl', 'mf', 'mb']

        commands_map = {'mf': self.move_forward, 'rr': self.rotate_right,
                        'rl': self.rotate_left, 'mb': self.move_backward}

        for c in cms:
            commands_map[c]()

