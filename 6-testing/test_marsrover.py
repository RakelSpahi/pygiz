from marsrover import Rover

# move forward, back, rotate right, rotate left, take commands

def test_rover_exists():
    r = Rover()
    assert r.facing == 'n'
    assert r.pos == [0, 0]

def test_move_forward():
    r = Rover()

    assert r.pos == [0, 0]

    r.facing = 'n'
    r.move_forward()

    assert r.pos == [0, 1]

    r.facing = 'e'

    r.move_forward()
    assert r.pos == [1, 1]

    r.facing = 's'
    r.move_forward()
    assert r.pos == [1, 0]

    r.facing = 'w'
    r.move_forward()
    assert r.pos == [0, 0]

def test_rotate_right():
    r = Rover()

    r.facing = 'n'
    r.rotate_right()
    assert r.facing == 'e'

    r.rotate_right()
    assert r.facing == 's'

    r.rotate_right()
    assert r.facing == 'w'

def test_rotate_left():
    r = Rover()

    r.facing = 'n'
    r.rotate_left()
    assert r.facing == 'w'

    r.rotate_left()
    assert r.facing == 's'

    r.rotate_left()
    assert r.facing == 'e'

def test_move_backward():
    r = Rover()

    assert r.pos == [0, 0]

    r.facing = 'n'
    r.move_backward()

    assert r.pos == [0, -1]

    r.facing = 'e'

    r.move_backward()
    assert r.pos == [-1, -1]

    r.facing = 's'
    r.move_backward()
    assert r.pos == [-1, 0]

    r.facing = 'w'
    r.move_backward()
    assert r.pos == [0, 0]

def test_commands():
    r = Rover()
    r.pos = [0, 0]
    assert r.facing == 'n'

    r.commands(['mf', 'mf', 'rr', 'mf', 'rl', 'mf', 'mb'])

    assert r.facing == 'n'
    assert r.pos == [1, 2]


# back at 6:31
