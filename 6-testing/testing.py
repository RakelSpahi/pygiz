
"""

A test is a simple something that verifies that a piece of code works the way it should.

Testing is usually automated. In continuous integration, tests make sure that the source code a team works on
has no defects.

We can split testing into two types: Unit testing and integration testing.

Unit testing is done with individual pieces of functionality.
Integration tests ensure that multiple pieces of functionality work well together.

In practice, it's fairly simple:

"""

def add_numbers(x, y=10):
    return x + y

def subtract(x, y):
    return x - y

# To test this, we write the following code:

def test_add_numbers():
    assert add_numbers(10) == 20
    assert add_numbers(10, 20) == 30

def test_subtract():
    assert subtract(20, 10) == 10

"""

As we can see, our test is simply asserting that the function returns what we
expect it to.

It is often a good idea to write multiple cases for a test, including edge (rare) cases.

This is, of course, a simple example. The topic of testing deserves an entire course of its own.

Python comes with a testing library called Unittest, but we'll be using Pytest instead,
as it allows for automatic test discovery.

What this means is that Pytest will search for specific words in your file names and
functions to discover tests. For more info, refer to the docs:

https://docs.pytest.org/en/latest/goodpractices.html#test-discovery

"""



