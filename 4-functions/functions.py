
"""

Functions are one of the easiest things to learn in programming.

We define them with the def keyword and then we specify its parameters, if any.

"""

#           parameters
def addition(x, y):
    # notice the return statement
    return x + y


# To use the function, we call/invoke it like this:


z = addition(10, 20)

#print(z)

"""

Notice that for every parameter the function has, we have to pass in an argument, otherwise we get an error.

We can also specify default arguments, like this:


"""
def subtraction(x, y=10):
    print(x-y)
    return

z = subtraction(10)

print(z)

# Now, we can do one of two things:

#print(subtraction(10))

# or

#print(subtraction(10, 5))

# Notice how the default argument is being overriden when we pass in the second argument.

"""

def multiplication():
    return 10 * 20

a = multiplication(50)

print(a)



