
Very small list of useful and common bash commands. Anything inside of <> should be replaced with
an actual argument.

ls
clear
mkdir <dirname>
cd <dirname>
mv <file_name> <destination> (note: If name is different, renames file)
cp <file_name> <destination>
rm <file_name>
rm -r <dir_name>
touch <new_file_name>
