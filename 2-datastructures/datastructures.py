
"""

Data structures are used to organize data. We'll be working with 3 types for now: Lists, Tuples, and Dictionaries


Lists are arrays that hold data, and they contain an index that references every item it contains.

Indices start from 0, and increase by 1 as you move on in the list.

x = [10, 20, 30]

To retrieve 30, we access index 2 of x, like this:

x[2]

We could also access the list in reverse, so for 30, we would do:

x[-1]

For 20, we could do either:

x[1]

or

x[-2]

We can do many things with a list. We could, for example, add things, remove things, or modify things.

Some of the methods we can use include .append, .extend, .remove., .pop.

"""

x = [2, 5, 10]

x.append(50)

print(x)

"""

Tuples are similar to lists, except that that you use ( ) instead of [ ] to create them.

The other difference is that they're immutable, meaning you can't change anything about them.

"""

y = ('hello', 'goodbye')

y.append('okay')

# an error would result

"""

Dictionaries are different than lists and tuples in a few ways.

1. They're hash maps (key-value stores) instead of arrays.

2. They're accessed by their key rather than their index.

3. Syntax is different

"""

z = {'name': 'Bob', 'age': 1000}

print(z['age'])






